﻿using UnityEngine;
using System.Collections;

public class Caso : Object
{

    public int randomNumber;
    public int idRegra;
    public int idCaso;
    public string modelo;
    public string falaConsumidor;
    public string questionamento;
    public string respostaQuestionamento;
    public string parametros;
    public bool temDireito;

    public string var1;
    public string var2;

    public string falaSim;
    public string falaNao;

}