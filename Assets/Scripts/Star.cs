﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Star : MonoBehaviour {

    public Sprite spriteOn;
    public Sprite spriteOff;
    public Image imageComponent;

    public AudioSource _as;
    public AudioClip _ac;
    public ParticleSystem _p;

    public void TurnOn()
    {
        imageComponent.sprite = spriteOn;

        _as.clip = _ac;
        _as.Play();

        _p.Play();
    }

    public void TurnOn(float delay)
    {
        Invoke("TurnOn", delay);
    }
}
