﻿using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;
using System.Collections;

public class GameController : Common {

    public TextAsset jsonFile;
    public TextAsset jsonLevels;

    public int queue = 10;
    public int maxRules = 0;
    public int currentLevel = 0;

    public Monster monster;

    public GameObject[] disable;

    public Animator[] anims;
    public float levelTotalTime = 1f;

    public RectTransform answerPanel;
    public RuleBook book;

    public GameObject tableModelImg;
    public GameObject ovelayLivro;
    public GameObject ovelayModelo;
    public Transform ovelayModeloArea;
    public GameObject ovelayPause;
    public GameObject ovelayEndGame;
    public Alert ovelayAlert;

    public Ballon BallonConsumidor;
    public Ballon BallonJogador;


    public Text queueLabel;
    public Text dateLabel;
    public Text txtEndLevel;

    public string[] saudacoes;
    public string[] falasSim;
    public string[] falasNao;

    public System.DateTime initalDate = new System.DateTime(2053, 4, 1, 9, 0, 0, 0);
    public Star[] starSprites;
    public int starsCount = 3;
    public int errorsCount = 0;

    public Button[] btnsDisableOnWalk;

    public AudioSource _as;
    public AudioClip[] voicesAudio;
    public AudioClip[] soundFx;


    // Use this for initialization
    void Start() {

        currentLevel = PlayerPrefs.GetInt("currentLevel");
        getLevelInfo();

        ovelayLivro.SetActive(true);
        book.maxRules = maxRules;
        book.Init();
        
        answerPanel.anchoredPosition = new Vector2(0, -250);

        for (int i = 0; i < disable.Length; i++)
        {
            disable[i].SetActive(false);
        }


        tableModelImg.SetActive(false);
        ovelayAlert.gameObject.SetActive(true);

        int updateDay = PlayerPrefs.GetInt("currentLevel");

        if (updateDay >= 5) updateDay += 2;
        if (updateDay >= 12) updateDay += 2;

        initalDate = initalDate.AddDays(updateDay);
        dateLabel.text = System.String.Format("{0:dd/MM/yyyy}", initalDate);

        System.GC.Collect();

        EnableDisableBtns(false);
        
        blackSpot.turnTransparent(1f);

        for (int i = 0; i < anims.Length; i++)
            anims[i].speed = 0;

        InitLevel();
        
    }

    void getLevelInfo()
    {
        var json = JSON.Parse(jsonLevels.text);
        
        maxRules = int.Parse(json[currentLevel]["maxRules"].Value);
        levelTotalTime = float.Parse(json[currentLevel]["time"].Value);
        queue = int.Parse(json[currentLevel]["queue"].Value);
    }

    void UpdateQueue(int v)
    {
        queue -= v;
        queueLabel.text = "Fila: " + queue;
    }

    void UpdateQueue() { UpdateQueue(0); }

    void InitLevel()
    {
        var json = JSON.Parse(jsonFile.text);

        string nome = json[maxRules - 1]["nome"];
        string explicacao = json[maxRules - 1]["explicacao"];

        if (PlayerPrefs.GetInt("currentLevel")==23)
        {
            Alert("Parabéns! Você já aprendeu todas as regras e já está apto à iniciar seu trabalho. Vá em frente, os consumidores estão te esperando!");
        } else
        {
            Alert("Bem vindo ao seu " + (PlayerPrefs.GetInt("currentLevel") + 1) + "º dia de treinamento!\n Hoje você irá aprender e aplicar uma nova regra: \n\n<b>" + nome.ToUpper() + "</b>\n\n Veja o Livro de Regras para obter mais informações sobre essa regra.");
        }
        
    }

    public void StartLevel()
    {
        AdjustLevelTime();
        CallNextMonster();
    }

    public bool timeStarted = false;
    void AdjustLevelTime()
    {
        for (int i = 0; i < anims.Length; i++)
        {
            anims[i].speed = 1 / levelTotalTime;
        }

        Invoke("EndLevelByTime", 60f * levelTotalTime);
        Invoke("EndBusinessHours", 60f * levelTotalTime * .8f);

        timeStarted = true;
    }

    void EndBusinessHours()
    {
        starsCount--;
        print("Acabou o horário comercial!");
    }

    void EndLevelByTime()
    {
        starsCount = 0;
        EndLevel("O dia acabou e você não conseguiu atender todos os consumidores!");
    }

    // Update is called once per frame
    void Update() {

        base.Update();

        if (Input.GetKeyDown(KeyCode.F))
        {
            starsCount = 0;
            EndLevel("TESTE");
        }
    }

    public void CallNextMonster()
    {
        if (queue > 0)
        {
            UpdateQueue(1);
            GetRandomCase();
            monster.enterRoom();
        } else
        {
            if (starsCount == 3)
            {
                EndLevel("Parabéns, você atendeu todos os consumidores com perfeição!");
            } else
            {
                EndLevel("Parabéns, você atendeu todos os consumidores! Mas preste mais atenção para não errar.");
            }

        }
    }

    public void MonsterTalk()
    {
        EnableDisableBtns(true);
        BallonConsumidor.showBallon();
    }


    public Caso currentCase;
    int caseCount = 0;
    public void GetRandomCase()
    {
        var json = JSON.Parse(jsonFile.text);
        currentCase = new Caso();

        int currentIndex;
        
        if (PlayerPrefs.GetInt("currentLevel") < 23 && caseCount == 0 )
        {
            currentIndex = maxRules - 1;
        } else
        {
            currentIndex = Random.Range(0, maxRules);
        }

        //FORÇA SEMPRE CASOS DO MESMO DIA
        //currentIndex = maxRules - 1;


        int caseIndex = Random.Range(0, json[currentIndex]["casos"].Count);
        currentCase.idRegra = currentIndex;
        currentCase.idCaso = caseIndex;

        currentCase.falaConsumidor = json[currentIndex]["casos"][caseIndex]["fala_consumidor"].Value;
        currentCase.questionamento = json[currentIndex]["casos"][caseIndex]["questionamento"].Value;
        currentCase.respostaQuestionamento = json[currentIndex]["casos"][caseIndex]["resposta_questionamento"].Value;
        currentCase.parametros = json[currentIndex]["casos"][caseIndex]["parametros"].Value;
        currentCase.modelo = json[currentIndex]["casos"][caseIndex]["modelo"].Value;

        currentCase.falaSim = json[currentIndex]["casos"][caseIndex]["fala_sim"].Value;
        currentCase.falaNao = json[currentIndex]["casos"][caseIndex]["fala_nao"].Value;

        currentCase.temDireito = json[currentIndex]["casos"][caseIndex]["tem_razao"].Value == "1";

        var var1 = json[currentIndex]["casos"][caseIndex]["var1"];
        var var2 = json[currentIndex]["casos"][caseIndex]["var2"];

        int var1Index = Random.Range(0, var1.Count);
        int var2Index = var1.Count == var2.Count ? var1Index : Random.Range(0, var2.Count);

        currentCase.var1 = var1[var1Index];
        currentCase.var2 = var2[var2Index];

        currentCase.falaConsumidor = ReplaceText(currentCase.falaConsumidor);
        currentCase.questionamento = ReplaceText(currentCase.questionamento);
        currentCase.respostaQuestionamento = ReplaceText(currentCase.respostaQuestionamento);

        currentCase.randomNumber = Random.Range(1, 90);

        BallonConsumidor.ChangeVars();
        BallonConsumidor.ChangeText(currentCase.falaConsumidor);

        caseCount++;

    }

    string ReplaceText(string txt)
    {
        txt = txt.Replace("%saudacao%", saudacoes[Random.Range(0, saudacoes.Length)]);
        txt = txt.Replace("%var1%", currentCase.var1);
        txt = txt.Replace("%var2%", currentCase.var2);

        

        if (txt.IndexOf("DataMaiorQueHoje")>0)
        {
            System.DateTime date;
            date = initalDate.AddDays(Random.Range(1,20));
            txt = txt.Replace("DataMaiorQueHoje", System.String.Format("{0:dd/MM/yyyy}", date));
        }

        if (txt.IndexOf("DataMenorQueHoje") > 0)
        {
            System.DateTime date;
            date = initalDate.AddDays(-Random.Range(1, 20));
            txt = txt.Replace("DataMenorQueHoje", System.String.Format("{0:dd/MM/yyyy}", date));
        }

        return txt;
    }

    bool lastAnswer;
    public void checkAnswer(bool clicked)
    {

        EnableDisableBtns(false);

        if (clicked)
        {
            BallonJogador.ChangeText(currentCase.falaSim);
        } else
        {
            BallonJogador.ChangeText(currentCase.falaNao);
        }

        BallonConsumidor.hideBallon();
        BallonJogador.showBallon();

        if (clicked==currentCase.temDireito)
        {
            print("Acertou!");
        } else
        {
            if (errorsCount==0)
                starsCount--;
            
            errorsCount++;

            if (errorsCount < 3)
            {
                Alert("Preste mais atenção! Você está dando informações erradas aos consumidores.", 4f);
            } else if (errorsCount == 3)
            {
                Alert("Você já cometeu 3 erros hoje. Não vou admitir mais erros!!!", 4f);
            } else if (errorsCount > 3)
            {
                starsCount = 0;
                EndLevel("Você errou mais de 3 vezes hoje. Preste mais atenção!");
            }
        }

        tableModelImg.SetActive(false);
        toggleAnswerPanel();

        lastAnswer = clicked;

        Invoke("hideBallonJogador", 1.5f);
        Invoke("showBallonConsumidor", 1.5f);
        Invoke("EndTalk", 3f);
        //monster.exitRoom();

    }

    void showBallonConsumidor() {
        if (lastAnswer)
        {
            BallonConsumidor.ChangeText(falasSim[ Random.Range(0, falasSim.Length) ]);
        } else
        {
            BallonConsumidor.ChangeText(falasNao[Random.Range(0, falasNao.Length)]);
        }
        
        BallonConsumidor.showBallon();
    }
    void hideBallonJogador() { BallonJogador.hideBallon(); }

    public void EndTalk()
    {
        BallonConsumidor.hideBallon();
        BallonJogador.hideBallon();
        monster.exitRoom();
    }

    public void toggleAnswerPanel()
    {

        Vector2 dest = Vector2.zero;
        iTween.EaseType ease;
        float vel;
        if (answerPanel.anchoredPosition.y<100)
        {
            dest = new Vector2(0, 140);
            ease = iTween.EaseType.easeOutBack;
            vel = .5f;
        } else
        {
            dest = new Vector2(0, -250);
            ease = iTween.EaseType.easeInBack;
            vel = .3f;
        }

        iTween.ValueTo(answerPanel.gameObject, iTween.Hash(
            "from", answerPanel.anchoredPosition,
            "to", dest,
            "time", vel,
            "easetype", ease,
            "onupdatetarget", this.gameObject,
            "onupdate", "MoveGuiElement")
        );
    }

    public void MoveGuiElement(Vector2 position)
    {
        answerPanel.anchoredPosition = position;
    }


    bool inQuestion = false;
    public void doQuestion(int ruleIndex)
    {

        PlayAudio(5);
        ovelayLivro.SetActive(false);

        if (ruleIndex == currentCase.idRegra)
        {

            BallonConsumidor.hideBallon();
            BallonJogador.ChangeText(currentCase.questionamento);
            BallonJogador.showBallon(0.5f);

            inQuestion = true;



        } else
        {
            Alert("Ei, preste mais atenção. Essa não é a regra certa!");
        }
        
    }

    public void showModel()
    {

        BallonConsumidor.hideBallon();

        ovelayLivro.SetActive(false);
        ovelayModelo.SetActive(true);

        foreach (Transform child in ovelayModeloArea)
        {
            Destroy(child.gameObject);
        }

        print(currentCase.modelo);
        GameObject instance = Instantiate(Resources.Load(currentCase.modelo, typeof(GameObject))) as GameObject;
        instance.transform.SetParent(ovelayModeloArea, false);

        System.GC.Collect();
        Resources.UnloadUnusedAssets();
    }

    public void BallonPlayerClick()
    {
        BallonJogador.hideBallon();

        if (inQuestion)
        {
            BallonConsumidor.ChangeText(currentCase.respostaQuestionamento);
            BallonConsumidor.showBallon(0.5f);

            if (currentCase.modelo != "")
            {
                tableModelImg.SetActive(true);
                monster.anim.SetTrigger("MoveHand");
                StartCoroutine(loadTableModleImg());
            }

            inQuestion = false;
        }

        
    }

    IEnumerator loadTableModleImg()
    {
        print("Img" + currentCase.modelo);
        ResourceRequest request = Resources.LoadAsync<Sprite>("Img"+currentCase.modelo);
        yield return request;
        tableModelImg.GetComponent<Image>().overrideSprite = request.asset as Sprite;

        Resources.UnloadUnusedAssets();
    }

    public void resetModelRotation()
    {
        iTween.RotateTo(ovelayModeloArea.GetChild(0).gameObject, iTween.Hash(
            "rotation", new Vector3(-90,-180,0),
            "time", .5f,
            "easetype", iTween.EaseType.easeInOutCubic
            ));
    }

    public void EnableDisableBtns(bool enable)
    {
        for(int i=0; i<btnsDisableOnWalk.Length; i++)
        {
            btnsDisableOnWalk[i].interactable = enable;
        }
    }


    public void Alert(string text)
    {
        ovelayAlert.showAlert(text);
    }

    string nextAlert;
    public void Alert(string text, float delay)
    {
        nextAlert = text;
        Invoke("delayedAlert", delay);
    }

    void delayedAlert()
    {
        ovelayAlert.showAlert(nextAlert);
    }

    public void PauseGame()
    {
        Time.timeScale = 0;
        ovelayPause.SetActive(true);
    }

    public void ContinueGame()
    {
        Time.timeScale = 1;
        ovelayPause.SetActive(false);
    }

    public Button BtnNextLevel;
    void EndLevel(string text)
    {

        _as.clip = soundFx[0];
        _as.Play();

        txtEndLevel.text = text;

        //SALVA O PROGRESSO DO JOGADOR NA FASE
        if (PlayerPrefs.GetInt("stars_day_" + currentLevel) < starsCount)
        {
            PlayerPrefs.SetInt("stars_day_" + currentLevel, starsCount);
        }
        

        for (int i=0; i<starsCount; i++)
        {
            starSprites[i].TurnOn(.5f + i*.3f);
        }

        if (starsCount == 0)
        {
            BtnNextLevel.interactable = false;
        }

        ovelayEndGame.SetActive(true);
    }

    public void NextLevel()
    {
        PlayerPrefs.SetInt("currentLevel", PlayerPrefs.GetInt("currentLevel")+1);
        ChangeScene("Game");
    }

    public void PlayAudio(int index)
    {
        _as.clip = soundFx[index];
        _as.Play();
    }

}
