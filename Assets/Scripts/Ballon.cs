﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Ballon : MonoBehaviour {

    public GameController _gc;
    public AudioSource _as;
    public Animator anim;
    public Image baloonImg;
    public Text baloonText;

    string MenorQueSete;
    string MaiorQueSete;

    void Start()
    {
        hideBallon(true, 0);
    }

    public void ChangeVars()
    {
        MenorQueSete = Random.Range(2, 7).ToString();
        MaiorQueSete = Random.Range(8, 30).ToString();
    }

    public void ChangeText(string text)
    {

        if (text.IndexOf("%MenorQueSete%") > 0)
            text = text.Replace("%MenorQueSete%", MenorQueSete );

        if (text.IndexOf("%MaiorQueSete%") > 0)
            text = text.Replace("%MaiorQueSete%", MaiorQueSete);

        baloonText.text = text;
    }

    public void hideBallon(bool animate, float delay)
    {

        if (!animate)
        {
            foreach (Transform child in baloonImg.transform)
                child.gameObject.SetActive(false);

            baloonImg.enabled = false;

        }
        else
        {
            Hashtable ht = new Hashtable();

            ht.Add("scale", Vector3.one * .05f);
            ht.Add("easetype", iTween.EaseType.easeInBack);
            ht.Add("time", delay);
            ht.Add("oncomplete", "CompleteHide");
            ht.Add("oncompletetarget", gameObject);


            iTween.ScaleTo(baloonImg.gameObject, ht);
        }

    }

    void CompleteHide()
    {
        hideBallon(false);
    }

    public void hideBallon(float delay) { hideBallon(true, delay); }
    public void hideBallon(bool animate) { hideBallon(animate, .5f); }
    public void hideBallon() { hideBallon(true, .5f); }

    public void showBallon()
    {
        showBallon(0f);
    }

    public void showBallon(float delay)
    {
        
        foreach (Transform child in baloonImg.transform)
            child.gameObject.SetActive(true);


        Hashtable ht = new Hashtable();

        ht.Add("scale", Vector3.one);
        ht.Add("easetype", iTween.EaseType.easeOutBack);
        ht.Add("time", .5f);
        ht.Add("delay", delay);
        ht.Add("onstart", "startShowBallon");
        
        iTween.ScaleTo(baloonImg.gameObject, ht);
        

        if (_as!=null && _gc!=null)
        {
            if (baloonText.text.Length < 50)
            {
                _as.clip = _gc.voicesAudio[Random.Range(3, 9)]; 
            } else
            {
                _as.clip = _gc.voicesAudio[Random.Range(1, 3)];
            }
            _as.Play();

            if (anim != null)
            {
                Invoke("startLipAnim", .5f);
                Invoke("stopLipAnim", _as.clip.length*.75f-.5f);
            }
        }


    }


    void startShowBallon()
    {
        baloonImg.rectTransform.localScale = new Vector3(.2f, .2f, .2f);
        baloonImg.enabled = true;
    }
    

    void startLipAnim()
    {
        anim.SetBool("Talk", true);
    }

    void stopLipAnim()
    {
        anim.SetBool("Talk", false);
    }
}
