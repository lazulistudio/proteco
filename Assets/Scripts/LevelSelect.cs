﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelSelect : Common {

    public Sprite empty;
    public Sprite currentDay;
    public Sprite stars_1;
    public Sprite stars_2;
    public Sprite stars_3;
    public GameObject[] daysButtons;
    public GameObject btnWork;

    // Use this for initialization
    void Start () {
        ResetDays();
        EnableDays();

        base.Start();

        if (!PlayerPrefs.HasKey("stars_day_"+(daysButtons.Length-1)))
        {
            btnWork.SetActive(false);
        }
    }
	
	// Update is called once per frame
	void ResetDays () {
	    for(int i=0; i<daysButtons.Length; i++)
        {
            daysButtons[i].GetComponent<Image>().sprite = empty;
            daysButtons[i].GetComponent<Button>().interactable = false;
        }
	}

    void EnableDays()
    {
        int i;
        for (i = 0; i < daysButtons.Length; i++)
        {
            if (PlayerPrefs.HasKey("stars_day_" + i) && PlayerPrefs.GetInt("stars_day_" + i)>0)
            {
                switch(PlayerPrefs.GetInt("stars_day_" + i))
                {
                    case 1:
                        daysButtons[i].GetComponent<Image>().sprite = stars_1;
                        break;

                    case 2:
                        daysButtons[i].GetComponent<Image>().sprite = stars_2;
                        break;

                    case 3:
                        daysButtons[i].GetComponent<Image>().sprite = stars_3;
                        break;
                }


                daysButtons[i].GetComponent<Button>().interactable = true;
            } else
            {
                break;
            }
        }

        if (daysButtons.Length > i)
        {
            daysButtons[i].GetComponent<Image>().sprite = currentDay;
            daysButtons[i].GetComponent<Button>().interactable = true;
        }
                
    }

    public void StartLevel(int i)
    {
        PlayerPrefs.SetInt("currentLevel", i);
        ChangeScene("Game");
    }
}
