﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Menu : Common {

    public GameObject[] disable;
    public Toggle BtnAudio;

    // Use this for initialization
    void Start () {

        for (int i = 0; i < disable.Length; i++)
        {
            disable[i].SetActive(false);
        }

        if (!PlayerPrefs.HasKey("Audio"))
            PlayerPrefs.SetInt("Audio", 1);

        if (PlayerPrefs.GetInt("Audio") == 1)
        {
            BtnAudio.isOn = false;
        } else
        {
            BtnAudio.isOn = true;
        }


        base.Start();

    }

    public void ToggleAudio()
    {
        if (BtnAudio.isOn)
        {
            AudioListener.volume = 0;
            PlayerPrefs.SetInt("Audio", 0);
        } else
        {
            AudioListener.volume = 1;
            PlayerPrefs.SetInt("Audio", 1);
        }
    }

    public void DeletePlayerPrefs()
    {
        PlayerPrefs.DeleteAll();
        ChangeScene("Menu");
    }

    public void CheckPlayOrInstructions()
    {
        
        if (PlayerPrefs.HasKey("instructionsFinished") && PlayerPrefs.GetInt("instructionsFinished") == 1)
        {
            ChangeScene("LevelSelect");
        } else
        {
            ChangeScene("Instructions");
        }
    }

    int countClick = 0;
    public void Cheat()
    {
        countClick++;
        print(countClick);

        if (countClick==15)
        {
            for(int i=0; i<23; i++)
            {
                PlayerPrefs.SetInt("stars_day_" + i, 3);
            }
            disable[2].SetActive(true);
            
        }
    }
	
}
