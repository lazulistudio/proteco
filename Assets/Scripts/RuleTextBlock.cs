﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class RuleTextBlock : MonoBehaviour {

    public int index;
    public Text titulo;
    public Text explicacao;
    public Text fundamento;
    public RectTransform btnQuestionar;
    public RectTransform separador;

    public float height = 0f;
    float lastHeight = 0f;

    // Use this for initialization
    public IEnumerator Init(int _index, string _titulo, string _explicacao, string _fundamento )
    {
        index = _index;
        titulo.text = _titulo;
        explicacao.text = _explicacao;
        fundamento.text = _fundamento;

        yield return new WaitForEndOfFrame();
        
        fundamento.rectTransform.anchoredPosition = new Vector2(0, -110-explicacao.rectTransform.rect.height);
        btnQuestionar.anchoredPosition = new Vector2(btnQuestionar.anchoredPosition.x, -80-explicacao.rectTransform.rect.height);

        height = -btnQuestionar.anchoredPosition.y + btnQuestionar.rect.height;

        separador.anchoredPosition = new Vector2(0, -(height+40));

        GetComponent<RectTransform>().sizeDelta = new Vector2(GetComponent<RectTransform>().sizeDelta.x, height);
    }

    public void QuestionBtn()
    {
        GameObject.Find("GameController").SendMessage("doQuestion", index);
    }
}
