﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ColorBlink : MonoBehaviour {

    public Image imgComponente;
    public Color blinkColor = Color.white;
    public Color initialColor = Color.white;
    public float time = 1f;

    // Use this for initialization
    void Start () {
        //imgComponente.color = blinkColor;
        fadeToBlink();
    }

    public void fadeToBlink() { 
        iTween.ValueTo(gameObject, iTween.Hash(
            "from", initialColor,
            "to", blinkColor,
            "time", time,
            "easetype", iTween.EaseType.linear,
            "onupdatetarget", this.gameObject,
            "onupdate", "MoveGuiElement",
            "oncompletetarget", this.gameObject,
            "oncomplete", "fadeToInitial")
        );
    }

    public void fadeToInitial()
    {
        iTween.ValueTo(gameObject, iTween.Hash(
            "from", blinkColor,
            "to", initialColor,
            "time", time,
            "easetype", iTween.EaseType.linear,
            "onupdatetarget", this.gameObject,
            "onupdate", "MoveGuiElement",
            "oncompletetarget", this.gameObject,
            "oncomplete", "fadeToBlink")
        );
    }

    public void MoveGuiElement(Color c)
    {
        imgComponente.color = c;
    }
	
}
