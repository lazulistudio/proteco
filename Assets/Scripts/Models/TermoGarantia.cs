﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TermoGarantia : MonoBehaviour {

    public Text txtProduto;
    public Text txtData;

    GameController gc;
    void Start()
    {
        gc = GameObject.Find("GameController").GetComponent<GameController>();
        Init();
    }

    public void Init()
    {
        txtProduto.text = gc.currentCase.var1;

        System.DateTime date;

        if (gc.currentCase.parametros == "DataMaiorQueHoje")
        {
            date = gc.initalDate.AddDays(gc.currentCase.randomNumber);
            txtData.text = System.String.Format("{0:dd/MM/yyyy}", date);
        } else if (gc.currentCase.parametros == "DataMenorQueHoje")
        {
            date = gc.initalDate.AddDays(-gc.currentCase.randomNumber);
            txtData.text = System.String.Format("{0:dd/MM/yyyy}", date);
        }
    }
}
