﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Caixa : MonoBehaviour {

    public Renderer _r;
    GameController gc;

    public Image[] imagePlace;

    // Use this for initialization
    void Start () {
        gc = GameObject.Find("GameController").GetComponent<GameController>();

        for(int i=0; i<imagePlace.Length; i++)
        {
            imagePlace[i].gameObject.SetActive(false);
        }

        Init();
    }


    public void Init()
    {
        if (!gc.currentCase.temDireito)
        {
            imagePlace[Random.Range(0, imagePlace.Length)].gameObject.SetActive(true);
        }

        StartCoroutine(LoadTexture());
    }

    IEnumerator LoadTexture()
    {
        print("Texturas/Caixa " + gc.currentCase.var1);
        ResourceRequest request = Resources.LoadAsync("Texturas/Caixa "+ gc.currentCase.var1);
        yield return request;
        _r.material.mainTexture = request.asset as Texture2D;
    }
}
