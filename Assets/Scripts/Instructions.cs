﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Instructions : Common {

    public InstructionScreen[] telas;
    public Text pagLabel;
    public Button btnPrev;
    public Button btnNext;


    int currentIndex = 0;
    

	// Use this for initialization
	void Start () {

        for (int i = 0; i < telas.Length; i++)
        {
            telas[i].Hide(0);
        }

        ShowScreen();

        Invoke("BlackSpotTurnTransparent", .5f);


    }

    public void NextScreen()
    {
        currentIndex++;

        if (currentIndex < telas.Length)
        {
            ShowScreen();
        } else
        {

            if (PlayerPrefs.HasKey("instructionsFinished") && PlayerPrefs.GetInt("instructionsFinished") == 1)
            {
                ChangeScene("LevelSelect");
            } else
            {
                PlayerPrefs.SetInt("instructionsFinished", 1);
                PlayerPrefs.SetInt("currentLevel", 0);
                ChangeScene("Game");
            }
        }
            
    }

    public void PrevScreen()
    {
        currentIndex--;
        ShowScreen();
    }

    void ShowScreen()
    {

        pagLabel.text = (currentIndex + 1).ToString() + "/" + telas.Length.ToString();

        if (currentIndex == 0)
        {
            btnPrev.interactable = false;
        } else
        {
            btnPrev.interactable = true;
        }

        /*if (currentIndex >= telas.Length-1)
        {
            btnNext.interactable = false;
        }
        else
        {
            btnNext.interactable = true;
        }*/

        for (int i = 0; i < telas.Length; i++)
        {
            if (i <= currentIndex) {
                telas[i].Show();
            } else {
                telas[i].Hide();
            }
        }
    }
   
   
}
