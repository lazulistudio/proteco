﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Common : MonoBehaviour {

    public bool showOnStart;
    public BlackSpot blackSpot;

    public void Start()
    {
        if (showOnStart)
        {
            Invoke("BlackSpotTurnTransparent", .5f);
        }

        AudioListener.volume = PlayerPrefs.GetInt("Audio");

        Resources.UnloadUnusedAssets();
        System.GC.Collect();
    }

    
    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            print("SCREENSHOT!");
            Application.CaptureScreenshot("Screenshot" + Random.Range(0, 100) + ".png", 3);
        }

    }

    public void BlackSpotTurnTransparent()
    {
        blackSpot.turnTransparent();
    }

    public void BlackSpotTurnBlack()
    {
        blackSpot.turnBlack();
    }

    string nextScene;
    public void ChangeScene(string newScene)
    {
        Time.timeScale = 1;
        nextScene = newScene;
        BlackSpotTurnBlack();
        Invoke("ChangeSceneInvoke", .7f);
    }

    void ChangeSceneInvoke()
    {
        PlayerPrefs.SetString("nextScene", nextScene);
        SceneManager.LoadScene("Preloader");
    }

    public void QuitGame()
    {
        Application.Quit();
    }

}
