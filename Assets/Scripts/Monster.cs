﻿using UnityEngine;
using System.Collections;

public class Monster : MonoBehaviour {

    public GameController _gc;
    public Animator anim;
    public SkinnedMeshRenderer smr;
    public Transform[] pathIn;
    public Transform[] pathOut;
    public float velMonster = 3f;

    public int[] shapeZeroOrHundred;
    public int[] shapeZeroToHundred;
    public int[] shapeJustOne;

    public Renderer rend;
    public Texture[] TexturesUv;

    // Use this for initialization
    void Start () {

        RandomizeMonster();
        transform.position = pathIn[0].position;
       
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.A))
        {
            RandomizeMonster();
        }
	}

    void RandomizeMonster()
    {

        //RESETA TODOS OS VALORES
        int i = 0;
        for(i=0; i<smr.sharedMesh.blendShapeCount; i++)
        {
            smr.SetBlendShapeWeight(i, 0);
        }

        //ZERO OU 100
        for (i = 0; i < shapeZeroOrHundred.Length; i++)
        {
            smr.SetBlendShapeWeight(shapeZeroOrHundred[i], Random.Range(0, 2)*100);
        }

        //ZERO A 100
        for (i = 0; i < shapeZeroToHundred.Length; i++)
        {
            smr.SetBlendShapeWeight(shapeZeroToHundred[i], Random.Range(0.0f, 100.0f));
        }

        //APENAS UM
        smr.SetBlendShapeWeight(shapeJustOne[Random.Range(0, shapeJustOne.Length)], 100);


        //CORRIGE VALORES

        //FORÇA OVAL
        smr.SetBlendShapeWeight(1, 100);

        // SEM CHIFRE NAO PODE TER 1 CHIFRE
        if (smr.GetBlendShapeWeight(13)==100) smr.SetBlendShapeWeight(20, 0);

        //POSICAO DENTE DE RATO
        if (smr.GetBlendShapeWeight(9) > 70) smr.SetBlendShapeWeight(9, 70);


        rend.material.mainTexture = TexturesUv[Random.Range(0, TexturesUv.Length)];
    }

    public void enterRoom()
    {

        anim.SetBool("Walk", true);

        Hashtable ht = new Hashtable();

        ht.Add("path", pathIn);
        ht.Add("easetype", iTween.EaseType.linear);
        ht.Add("orienttopath", true);
        ht.Add("time", velMonster);
        ht.Add("oncomplete", "MonsterIn");
        ht.Add("axis", "y");
        

        transform.position = pathIn[0].position;
        transform.rotation = pathIn[0].rotation;
        transform.localScale = pathIn[0].localScale;

        iTween.MoveTo(gameObject, ht);


        ht = new Hashtable();

        ht.Add("scale", pathIn[pathIn.Length - 1].localScale);
        ht.Add("easetype", iTween.EaseType.linear);
        ht.Add("time", velMonster);

        iTween.ScaleTo(gameObject, ht);


    }

    public void exitRoom()
    {

        anim.SetBool("Walk", true);

        Hashtable ht = new Hashtable();

        ht.Add("amount", new Vector3(0, .5f, 0));
        ht.Add("easetype", iTween.EaseType.linear);
        ht.Add("time", 1f);

        iTween.RotateBy(gameObject, ht);


        ht = new Hashtable();

        ht.Add("path", pathOut);
        ht.Add("easetype", iTween.EaseType.linear);
        ht.Add("orienttopath", true);
        ht.Add("time", velMonster);
        ht.Add("delay", 1f);
        ht.Add("axis", "y");
        ht.Add("oncomplete", "MonsterOut");

        iTween.MoveTo(gameObject, ht);

        ht = new Hashtable();

        ht.Add("scale", pathIn[0].localScale);
        ht.Add("easetype", iTween.EaseType.linear);
        ht.Add("delay", 1f);
        ht.Add("time", velMonster);

        iTween.ScaleTo(gameObject, ht);
    }

    void MonsterIn()
    {
        stopWalk();
        iTween.RotateTo(gameObject, pathIn[pathIn.Length-1].rotation.eulerAngles, 1f);
        _gc.MonsterTalk();
    }

    void MonsterOut()
    {
        stopWalk();
        RandomizeMonster();
        _gc.CallNextMonster();
    }

    public void stopWalk()
    {
        anim.SetBool("Walk", false);
    }

}
