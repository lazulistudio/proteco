﻿using UnityEngine;
using System.Collections;

public class Radio : MonoBehaviour {

    public AudioSource _as;
    public AudioClip staticAudio;
    public string[] musicsNames;
    int index;

	// Use this for initialization
	void Start () {
        index = Random.Range(0, musicsNames.Length);
        ChangeMusic();
    }
	
	public void ChangeMusic()
    {

        if (staticAudio != null)
        {
            _as.clip = staticAudio;
            _as.Play();
        }
        

        StartCoroutine(LoadMusic());

        
    }
    
    IEnumerator LoadMusic()
    {

        index++;
        if (index >= musicsNames.Length)
        {
            index = 0;
        }

        ResourceRequest audio = Resources.LoadAsync("Musicas/" + musicsNames[index]);
        yield return audio;
        _as.clip = audio.asset as AudioClip;
        _as.Play();

        Resources.UnloadUnusedAssets();

    }
}
