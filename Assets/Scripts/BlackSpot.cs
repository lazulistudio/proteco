﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BlackSpot : MonoBehaviour {

    public Animator anim;

	public void turnBlack()
    {
        setActiveOn();
        anim.SetBool("Transparent", false);
    }

    public void turnBlack(float time)
    {
        Invoke("turnBlack", time);
    }

    public void turnTransparent()
    {
        anim.SetBool("Transparent", true);
    }

    public void turnTransparent(float time)
    {
        Invoke("turnTransparent", time);
    }

    public void setActiveOn()
    {
        GetComponent<Image>().enabled = true;
    }


    public void setActiveOff()
    {
        GetComponent<Image>().enabled = false;
    }
}
