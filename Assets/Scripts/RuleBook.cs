﻿using UnityEngine;
using System.Collections;
using SimpleJSON;

public class RuleBook : MonoBehaviour {

    public int maxRules;
    public TextAsset jsonFile;

    public GameObject scrollArea;
    public GameObject prefabTextBlock;
    
    float lastHeight = 0f;

    public void Init() { StartCoroutine(LoadRules()); }

    public IEnumerator LoadRules()
    {
        RuleTextBlock rtb;
        var json = JSON.Parse(jsonFile.text);
        for(int i=0; i< Mathf.Min(maxRules, json.Count); i++)
        {
            GameObject obj = Instantiate(prefabTextBlock) as GameObject;
            obj.transform.SetParent(scrollArea.transform, false);

            obj.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, lastHeight);

            rtb = obj.GetComponent<RuleTextBlock>();

            StartCoroutine(
                rtb.Init(i, json[i]["nome"], json[i]["explicacao"], json[i]["fundamento"])
            );

            while (rtb.height==0) {
                yield return new WaitForEndOfFrame();
            }

            lastHeight -= rtb.height + 100;
        }

        scrollArea.GetComponent<RectTransform>().sizeDelta = new Vector2(
            scrollArea.GetComponent<RectTransform>().sizeDelta.x,
            -lastHeight);

        transform.parent.gameObject.SetActive(false);

        yield return new WaitForEndOfFrame();
    }

}
