﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Alert : MonoBehaviour {

    public Image[] FadeObj;
    public Text[] FadeText;
    public Text TextArea;
    public float time = .5f;

    public RectTransform rcText;
    public RectTransform rcBallon;

    public AudioSource _as;
    public AudioClip[] _ac;
    public GameController _gc;

    public bool hideByTime = false;

    // Use this for initialization
    void Start () {
        //FadeOut(0);
    }

    public void showAlert(string text)
    {
        gameObject.SetActive(true);
        TextArea.text = text;

        Invoke("FitHeight", 0.1f);
        Invoke("FitHeight", 0.2f);
        Invoke("FitHeight", 0.4f);
        Invoke("FitHeight", 0.8f);

        FadeIn();
        if (hideByTime) HideAlert(5f);
    }

    public void HideAlert(float delay)
    {
        if (!_gc.timeStarted)
        {
            _gc.StartLevel();
        }
        CancelInvoke();
        Invoke("FadeOut", delay);
    }

    void FitHeight()
    {

        rcBallon.sizeDelta = new Vector2(rcBallon.sizeDelta.x, rcText.sizeDelta.y+120);
        
    }

    public void FadeOut()
    {
        FadeOut(time);
    }

    public void FadeOut(float t)
    {

        CancelInvoke();

        iTween.ValueTo(gameObject, iTween.Hash(
           "from", 1f,
           "to", 0f,
           "time", time,
           "onupdatetarget", this.gameObject,
           "onupdate", "UpdateColor",
           "oncomplete", "Hide")
       );

    }

    void Hide() { gameObject.SetActive(false); }

    public void FadeIn()
    {

        gameObject.SetActive(true);

        iTween.ValueTo(gameObject, iTween.Hash(
           "from", 0f,
           "to", 1f,
           "time", time,
           "onupdatetarget", this.gameObject,
           "onupdate", "UpdateColor",
           "oncomplete", "CompleteFadeIn")
       );

    }

    public void CompleteFadeIn()
    {
        _as.clip = _ac[Random.Range(0, _ac.Length)];
        _as.Play();
    }


    public void UpdateColor(float a)
    {
        for(int i=0; i< FadeObj.Length; i++)
        {
            FadeObj[i].color = new Color(FadeObj[i].color.r, FadeObj[i].color.g, FadeObj[i].color.b, a);
        }

        for (int i = 0; i < FadeText.Length; i++)
        {
            FadeText[i].color = new Color(FadeText[i].color.r, FadeText[i].color.g, FadeText[i].color.b, a);
        }

    }

}
