﻿using UnityEngine;
using System.Collections;

public class InstructionScreen : MonoBehaviour {

    RectTransform rc;

	// Use this for initialization
	void Start () {
        rc = GetComponent<RectTransform>();
	}

    public void Show()
    {

        if (rc == null)
        {
            rc = GetComponent<RectTransform>();
        }

        iTween.ValueTo(this.gameObject, iTween.Hash(
            "from", rc.anchoredPosition,
            "to", new Vector2(0, rc.anchoredPosition.y),
            "time", .5f,
            "easetype", iTween.EaseType.easeOutCubic,
            "onupdatetarget", this.gameObject,
            "onupdate", "MoveGuiElement")
        );
    }

    public void Hide() { Hide(.5f); }
    public void Hide(float time)
    {

        if (rc == null)
        {
            rc = GetComponent<RectTransform>();
        }

        iTween.ValueTo(this.gameObject, iTween.Hash(
            "from", rc.anchoredPosition,
            "to", new Vector2(rc.rect.width, rc.anchoredPosition.y),
            "time", time,
            "easetype", iTween.EaseType.easeOutCubic,
            "onupdatetarget", this.gameObject,
            "onupdate", "MoveGuiElement")
        );
    }

    public void MoveGuiElement(Vector2 position)
    {
        rc.anchoredPosition = position;
    }
}
