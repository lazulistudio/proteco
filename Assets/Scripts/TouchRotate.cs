﻿using UnityEngine;
using System.Collections;

public class TouchRotate : MonoBehaviour {

    public Vector3 multiply = Vector3.one;

    Vector2 lastPosition;
    void Update()
    {
        if (Input.touchCount > 0)
        {

            if(Input.GetTouch(0).phase != TouchPhase.Began)
            {
                Vector2 dif = Input.GetTouch(0).position-lastPosition;

                transform.Rotate(new Vector3(-dif.y*multiply.x, 0*multiply.y, -dif.x*multiply.z));
            }

            lastPosition = Input.GetTouch(0).position;
        }
    }
}
