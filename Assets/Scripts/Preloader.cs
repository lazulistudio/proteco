﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Preloader : MonoBehaviour {

    public Image imgPreloader;
    
    AsyncOperation async;

    // Use this for initialization
    void Start () {

        Resources.UnloadUnusedAssets();
        System.GC.Collect();
        
        StartCoroutine(LoadLevelName());
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(async.progress * 100);

        imgPreloader.fillAmount = async.progress;
    }

    IEnumerator LoadLevelName()
    {
        async = SceneManager.LoadSceneAsync(PlayerPrefs.GetString("nextScene"));
        yield return null;
    }

}
