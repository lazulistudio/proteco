﻿using UnityEngine;
using System.Collections;

public class Plant : MonoBehaviour {

    public Animator anim;

	// Use this for initialization
	void Start () {
	
	}
	
	public void PlayRandomAnim()
    {
        int r = Random.Range(1, 5);
        anim.SetInteger("Random", r);

        Invoke("ResetAnim", .3f);
    }

    public void ResetAnim()
    {
        anim.SetInteger("Random", 0);
    }
}
